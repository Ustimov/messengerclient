package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 08/11/15.
 */
public class ChatRoomListRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.CHANNELLIST;

    @Override
    public String getAction() {
        return mAction;
    }
}
