package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 08/11/15.
 */
public class EnterChatRoomRequest extends AuthData implements IRequest{

    private transient String mAction = Actions.ENTER;

    public String channel;

    public EnterChatRoomRequest(String channelId) {
        channel = channelId;
    }

    @Override
    public String getAction() {
        return mAction;
    }
}
