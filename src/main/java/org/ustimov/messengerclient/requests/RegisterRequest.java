package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;

/**
 * Created by ustimov on 08/11/15.
 */
public class RegisterRequest implements IRequest {

    private transient String mAction = Actions.REGISTER;

    public String login;
    public String pass;
    public String nick;

    public RegisterRequest(String login, String password, String nickname) {
        this.login = login;
        this.pass = password;
        this.nick = nickname;
    }

    @Override
    public String getAction() {
        return mAction;
    }

}
