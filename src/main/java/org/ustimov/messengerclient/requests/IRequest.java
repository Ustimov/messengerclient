package org.ustimov.messengerclient.requests;

/**
 * Created by ustimov on 08/11/15.
 */
public interface IRequest {
    public String getAction();
}
