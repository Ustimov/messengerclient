package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 08/11/15.
 */
public class LeaveChatRoomRequest extends AuthData implements IRequest{

    private transient String mAction = Actions.LEAVE;

    public String channel;

    public LeaveChatRoomRequest(String channelId) {
        channel = channelId;
    }

    @Override
    public String getAction() {
        return mAction;
    }

}
