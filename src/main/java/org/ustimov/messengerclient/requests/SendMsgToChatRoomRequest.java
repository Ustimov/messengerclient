package org.ustimov.messengerclient.requests;

import org.ustimov.messengerclient.Actions;
import org.ustimov.messengerclient.AuthData;

/**
 * Created by ustimov on 08/11/15.
 */
public class SendMsgToChatRoomRequest extends AuthData implements IRequest {

    private transient String mAction = Actions.MESSAGE;

    public String channel;
    public String body;

    public SendMsgToChatRoomRequest(String channelId, String message) {
        channel = channelId;
        body = message;
    }

    @Override
    public String getAction() {
        return mAction;
    }
}
