package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.requests.AuthRequest;

/**
 * Created by ustimov on 08/11/15.
 */
public class AuthResponse extends ResponseAggregator {

    public AuthResponse(String status, String error, String userId, String sessionId) {
        super(status, error);
        uid = userId;
        sid = sessionId;
    }

    public String getUserId() {
        return uid;
    }

    public String getSessionId() {
        return sid;
    }
}
