package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.models.Message;
import org.ustimov.messengerclient.models.User;

/**
 * Created by ustimov on 08/11/15.
 */
public class EnterChatRoomResponse extends ResponseAggregator {
    public EnterChatRoomResponse(String status, String error, User[] users, Message[] messages) {
        super(status, error);
        this.users = users;
        this.last_msg = messages;
    }
}
