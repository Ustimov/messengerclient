package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.models.Channel;

/**
 * Created by ustimov on 08/11/15.
 */
public class ChatRoomListResponse extends ResponseAggregator {
    public ChatRoomListResponse(String status, String error, Channel[] channels) {
        super(status, error);
        this.channels = channels;
    }
}
