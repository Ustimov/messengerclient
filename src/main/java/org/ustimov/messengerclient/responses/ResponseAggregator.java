package org.ustimov.messengerclient.responses;

import org.ustimov.messengerclient.events.IEvent;
import org.ustimov.messengerclient.models.Channel;
import org.ustimov.messengerclient.models.Message;
import org.ustimov.messengerclient.models.User;

/**
 * Created by ustimov on 14/11/15.
 */
public class ResponseAggregator implements IResponse, IEvent {

    protected String status;
    protected String error;
    public String sid;
    public String uid;
    public Channel[] channels;
    public User[] users;
    public Message[] last_msg;

    public String chid;
    public String from;
    public String nick;
    public String body;


    public ResponseAggregator() {}

    public ResponseAggregator(String status, String error) {
        this.status = status;
        this.error = error;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public String getError() {
        return error;
    }
}
