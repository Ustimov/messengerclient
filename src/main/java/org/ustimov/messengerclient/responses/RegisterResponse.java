package org.ustimov.messengerclient.responses;

/**
 * Created by ustimov on 08/11/15.
 */
public class RegisterResponse extends ResponseAggregator {
    public RegisterResponse(String status, String error) {
        super(status, error);
    }
}
