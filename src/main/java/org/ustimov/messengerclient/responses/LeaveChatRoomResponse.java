package org.ustimov.messengerclient.responses;

/**
 * Created by ustimov on 08/11/15.
 */
public class LeaveChatRoomResponse extends ResponseAggregator {
    public LeaveChatRoomResponse(String status, String error) {
        super(status, error);
    }
}
