package org.ustimov.messengerclient.models;

/**
 * Created by ustimov on 14/11/15.
 */
public class Channel {
    public String chid;
    public String name;
    public String descr;
    public String online;
}
