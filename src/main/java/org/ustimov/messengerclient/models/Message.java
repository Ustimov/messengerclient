package org.ustimov.messengerclient.models;

/**
 * Created by ustimov on 14/11/15.
 */
public class Message {
    public String mid;
    public String from;
    public String nick;
    public String body;
    public String time;
}
