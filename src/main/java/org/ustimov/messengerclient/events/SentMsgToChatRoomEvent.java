package org.ustimov.messengerclient.events;

import org.ustimov.messengerclient.requests.SendMsgToChatRoomRequest;
import org.ustimov.messengerclient.responses.ResponseAggregator;

/**
 * Created by ustimov on 08/11/15.
 */
public class SentMsgToChatRoomEvent extends ResponseAggregator implements IEvent {
    public SentMsgToChatRoomEvent(String channelId, String fromUserId, String nickname, String message) {
        super();
        chid = channelId;
        from = fromUserId;
        nick = nickname;
        body = message;
    }
}
