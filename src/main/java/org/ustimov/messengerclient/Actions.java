package org.ustimov.messengerclient;

import com.sun.xml.internal.rngom.parse.host.Base;
import org.ustimov.messengerclient.events.*;
import org.ustimov.messengerclient.responses.*;

import java.lang.reflect.Type;

/**
 * Created by ustimov on 08/11/15.
 */
public class Actions {
    public static final String REGISTER = "register";
    public static final String AUTH = "auth";
    public static final String CHANNELLIST = "channellist";
    public static final String USERINFO = "userinfo";
    public static final String ENTER = "enter";
    public static final String LEAVE = "leave";
    public static final String MESSAGE = "message";
    public static final String WELCOME = "welcome";
    public static final String EV_ENTER = "ev_enter";
    public static final String EV_LEAVE = "ev_leave";
    public static final String EV_MESSAGE = "ev_message";

    public static Type getType(String action) {
        switch (action) {
            case REGISTER:
                return RegisterResponse.class;
            case AUTH:
                return AuthResponse.class;
            case WELCOME:
                return WelcomeEvent.class;

        }
        return ResponseAggregator.class;
    }

    public static boolean isEvent(String action) {
        return action.equals(WELCOME) || action.substring(0, 2).equals("ev");
    }

    public static IResponse getResponse(String action, ResponseAggregator response) {
        switch (action) {
            case REGISTER:
                return new RegisterResponse(response.getStatus(), response.getError());
            case AUTH:
                return new AuthResponse(response.getStatus(), response.getError(), response.uid, response.sid);
            case CHANNELLIST:
                return new ChatRoomListResponse(response.getStatus(),response.getError(), response.channels);
            case ENTER:
                return new EnterChatRoomResponse(response.getStatus(), response.getError(),
                        response.users, response.last_msg);
            case LEAVE:
                return new LeaveChatRoomResponse(response.getStatus(), response.getError());
        }
        return response;
    }

    public static IEvent getEvent(String action, ResponseAggregator response) {
        switch (action) {
            case EV_ENTER:
                return new ChatRoomEnterEvent(response.chid, response.uid, response.nick);
            case EV_LEAVE:
                return new ChatRoomLeaveEvent(response.chid, response.uid, response.nick);
            case EV_MESSAGE:
                return new SentMsgToChatRoomEvent(response.chid, response.from, response.nick, response.body);
        }
        return response;
    }
}
